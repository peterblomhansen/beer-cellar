import { IbeerDetails } from "./BeerDetails";
import Link from 'next/link';
import { useState } from "react";
import styles from '../styles/Beer.module.css'


interface IbeerListItemProps {
    beer: IbeerDetails;
}

const BeerListItem = ({beer}: IbeerListItemProps) => {
    const [numBottles,setNumBottles] = useState(beer.stock);
    const changeStock = (changeAmount: number) => {
      setNumBottles(numBottles + changeAmount);
    }

    const title = `${beer.name } (${numBottles})`;


  return (
    <li className={styles.listitem}>
      <Link href={ `/beer/${beer.slug}` }>
        { title }
      </Link>
      <button onClick={ () => changeStock(1) }>Stock</button>
      { numBottles > 0 && <button onClick={ () => changeStock(-1) }>Drink</button> }
  </li>        
  )
}

export default BeerListItem;


