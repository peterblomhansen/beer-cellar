import Link from 'next/link'
import styles from '../styles/Beer.module.css'

export interface IbeerDetails {
  name: string;
  brewery: string;
  size: string;
  alcohol: number;
  image: string;
  organic: boolean;
  slug: string;
  stock: number;
  
}

const BeerDetails = ({name, brewery, size, alcohol, image, organic, stock}: IbeerDetails) => {
  return (
    <div className={styles.beerdetail}>
      <div>
        <Link href="/" >back</Link>
        <h1>{ name } { organic && "(Organic)" }</h1>
        <p>Brewery: { brewery }</p>
        <p>Size: { size } </p>
        <p>Alcohol: { alcohol }</p>
        
        <p>In stock: {stock }</p>
      </div>
      <img className={styles.image} src={ image } alt={name} />
    </div>
  )
}

export default BeerDetails