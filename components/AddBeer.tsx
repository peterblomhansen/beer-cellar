import styles from '../styles/AddBeer.module.css'

const AddBeer = () => {
    return (
        
        <div className={styles.form} >
            <input type='text' placeholder="name" /><br />
            <input type='text' placeholder="brewery" /><br />
            <input type='text' placeholder="alcohol" /><br />
            <input type='text' placeholder="organic" /><br />
            <button disabled >Add Beer</button> (feature under development)
        </div>
    )
}

export default AddBeer;