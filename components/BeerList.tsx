import { IbeerDetails } from "./BeerDetails";
import BeerListItem from "./BeerListItem";

interface IbeerListProps {
  beerlist: IbeerDetails[];
} 

const BeerList = ({beerlist}: IbeerListProps) => {

  return (
    <>
    <h1>Cellar overview</h1>
    <ul>
    { 
      beerlist.map((beer: IbeerDetails) => {
        const title = `${beer.name } (${beer.stock})`;
        const numberOfBottles = beer.stock; 
        return (
          <BeerListItem {...{ beer }} key={beer.slug} />
        )
      }) 
    }
    </ul>
    </>
)

}

export default BeerList;