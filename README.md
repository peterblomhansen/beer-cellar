# Beer Cellar
Impact Coding Challenge

## Overall approach
I wanted to challenge myself by using Nextjs, as this is the go to solution, that Impact uses.

I purely focused on the frontend breaking the application down into components and three different routes. Two of them are regular pages (/, /beer/[slug]/) and the last one is a simple simulation of a backend (/api/bber/).

## Learnings and shortcomings 

The pure frontend approach using a JSON object sitting inside a function made it hard for me to work with real data and async calls. 

I also used some time to wrap my head around the smart serverside rendering logic and chose to use time to make the pages prerender on the server.

Typescript tied med to a concise data model for the beers. On the top level though I encounters a type error that i solved by **any**, even though it is not optimal. 

I also used a short amout of time styling the solution with css modules primarily using css grid to get a quick thought not perfect result.

## Get it running 
```bash
npm install
npm run dev
```


------------------------

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
