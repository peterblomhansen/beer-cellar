// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { IbeerDetails } from '../../components/BeerDetails';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IbeerDetails[]>
) {

  const beers = [
    {
      "name": "Fynsk Forår",
      "brewery": "Refsvindinge",
      "size": "0.5 liter",
      "alcohol": 4.8,
      "image": "https://nemvin.com/wp-content/uploads/2020/08/fynsk-for%C3%A5r.jpg",
      "organic": true,
      "slug": "fynsk-foraar",
      "stock": 3
    },
    {
      "name": "Tuborg Classic",
      "brewery": "Tuborg",
      "size": "0.33 liter",
      "alcohol": 4.6,
      "image": "https://cdn.shopify.com/s/files/1/1485/8320/products/2406_0_12_71f770bb-6d09-4877-96f2-3681b45ed61a_1024x1024.jpg?v=1515748119",
      "organic": false,
      "slug": "tuborg-classic",
      "stock": 23
    },    
    {
      "name": "Nordic Pilsner",
      "brewery": "Carlsberg",
      "size": "0.33 liter",
      "alcohol": 0.01,
      "image": "http://www.drinko.se/media/catalog/product/cache/1/image/5b4ce2e974906a11297266e0d7bca680/c/a/carlsberg-nordic-alkoholfri.jpg",
      "organic": false,
      "slug": "carlsberg-nordic",
      "stock": 7
    }   

  ]

  res.status(200).json(beers)
}
