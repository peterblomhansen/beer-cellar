import { useRouter } from "next/dist/client/router"; 
import BeerDetails, { IbeerDetails } from "../../components/BeerDetails";


const BeerPage = (props: any) => {
  const router = useRouter();
  const slug = router.query.slug;

  if(slug) {

    const specificBeer: IbeerDetails = props.beers.find((beer: IbeerDetails) => {
      return beer.slug === slug;
    })

    if (specificBeer) {
      return (
        <BeerDetails {...props=specificBeer} />
      )
    }

    return (<h1>Beer not found</h1>)

  }

  return <>Loading</>;
  

}

export async function getServerSideProps() {
  // Boilerplate from official Nextjs document
  const res = await fetch(`http://localhost:3000/api/beer`)
  const data = await res.json()


  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      beers:data
    }, 

  }
  

}

export default BeerPage;