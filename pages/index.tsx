import type { NextPage } from 'next'
import { useState } from 'react'

import BeerList from '../components/BeerList'
import AddBeer from '../components/AddBeer'

const Home: NextPage = (props: any) => {
  const [showAddForm,SetShowAddForm] = useState(false);
  const beerlist = props.beer;
 
  return (
    <div >
      <BeerList {...props={beerlist}} />
      <button onClick={ () => SetShowAddForm(true)} >Add new Beer</button>
      { showAddForm && <AddBeer />}
    </div> 
  )
}


export async function getServerSideProps() {
  // Boilerplate from official Nextjs document
  const res = await fetch(`http://localhost:3000/api/beer`)
  const data = await res.json()
  
  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      beer: data
    }, 

  }
}

export default Home
